package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn) {
        HashMap<String, Double> pass = new HashMap<String, Double>();
        pass.put("1", 10.0);
        pass.put("2", 45.0);
        pass.put("3", 20.0);
        pass.put("4", 35.0);
        pass.put("5", 50.0);
        return pass.get(isbn);
    }
}
